import numpy as np

def calc_rot_mx(p0, px, py):
    vx = np.subtract(px,p0)/np.linalg.norm(np.subtract(px,p0))
    vy = np.subtract(py,p0)/np.linalg.norm(np.subtract(py,p0))
    vz = np.cross(vx, vy)
    return np.transpose([vx,vy,vz])

def task2robot(Ptask, R, P0):
    return np.add( np.matmul(R, Ptask), P0)

p0=[577, -722, -21]
px=[579, -477, -23]
py=[425, -720, -23]
ptask = [1, 1, 1]
R = calc_rot_mx(p0, px, py)

print(R)

probot = task2robot(ptask, R, p0)

print(probot)
