function R = calc_rot_mx(p0, px, py)
    vx = (px-p0)/norm(px-p0);
    vy = (py-p0)/norm(py-p0);
    vz = cross(vx,vy);
    R = [vx vy vz];
end